package com.conygre.training.ordersimulator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class OrderProcessingSimulatorApplication {

	public static void main(String[] args) {
		SpringApplication.run(OrderProcessingSimulatorApplication.class, args);
	}

}
